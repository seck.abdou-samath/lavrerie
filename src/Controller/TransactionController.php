<?php

namespace App\Controller;

use App\Entity\Transaction;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TransactionController extends AbstractController
{
    #[Route('/transaction', name: 'app_transaction')]
    public function index(EntityManagerInterface  $entityManager): Response
    {
      
        $tran = $entityManager->getRepository(Transaction::class)->findAll();

        return $this->render('transaction/index.html.twig', [
            'controller_name' => 'TransactionController',
            'tran' => $tran
        ]);
    }
}
