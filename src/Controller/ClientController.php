<?php

namespace App\Controller;

namespace App\Controller;

use App\Entity\Clients;
use App\Entity\Transaction;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\ClientType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\BrowserKit\Response;

class ClientController extends AbstractController
{
    #[Route('/client/new', name: 'client_new')]
    public function new(Request $request, EntityManagerInterface $entityManager)
    {
        $client = new Clients();
    
        // Créer le formulaire en utilisant la classe de formulaire correspondante (par exemple, ClientType)
        $form = $this->createForm(ClientType::class, $client);
    
        // Gérer la soumission du formulaire
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // Enregistrer le profil client dans la base de données
            $entityManager->persist($client);
            $entityManager->flush();
    
            // Rediriger l'utilisateur vers la page de profil client
            return $this->redirectToRoute('client_show', ['id' => $client->getId()]);
        }
    
        // Afficher le formulaire dans le template Twig
        return $this->render('client/new.html.twig', [
            'controller_name' => 'ClientController',
            'form' => $form->createView(),
        ]);
    }
    
    #[Route('/client/{id}', name: 'client_show')]
    public function show(Clients $client,EntityManagerInterface  $entityManager)
    {
        $tran = $entityManager->getRepository(Transaction::class)->findAll();
        return $this->render('client/show.html.twig', [
            'controller_name' => 'TransactionController',
            'client' => $client,
            'tran' => $tran,
        ]);
    }

   

}