<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'lavrerie' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', 'root' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'KI yw9s|xk8w&,z[!+/yuy[c`CZXWoLw[Yt)L3HnPocb2=kwfq;O/M7-o/~Pl*S#' );
define( 'SECURE_AUTH_KEY',  'ci?`6m<N!n,/_aVD!X<18q=X<]sh+,>UMsOk0cW3j2_h}^+>NXW7qz7/!*y@- T2' );
define( 'LOGGED_IN_KEY',    'Vw?m#)@^Z^D`$KlgL:4H3KFkA>MWl|=?p/}V JF T7:^8>!1 ^yFM%@kxuor]~M,' );
define( 'NONCE_KEY',        '-in=&GUZrrNu#S>OSlb%1<T2^3u#Ljyp:;Vo|1z_wKOF0``D=<mRO{w}5Zoh4@ti' );
define( 'AUTH_SALT',        '*t^OU30upXwP-u@B/1E>nC2/&)t;B{p)g !f58gnN;0~XWo:rQi]Qcs$h}vX{TE&' );
define( 'SECURE_AUTH_SALT', '@72g5 P}}HP1Phhh;wSFM[dqmP0y>8-F`B=l8:?Ej@7ja)C44C)N.TfkMW5s)uY!' );
define( 'LOGGED_IN_SALT',   'b4sN6Zp82z|fB[X/a}`/q@Xd-LJ9aS=Y+TPO;nv)P}I}f#4NZQud4w6{)@^tob .' );
define( 'NONCE_SALT',       '{ g^[.-VT{f]A>+eFmpi5.7cKy,!o9Ur;rksZ IR~mNo_kA+])q>&ec1Y0i^MpS?' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortement recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
